import json

import requests

result = requests.post('http://127.0.0.1:8000/create_owner_and_wallet/',
                       json={'login': 'el_greco', 'name': 'El Greco'}).text
print(result)
owner1_id = json.loads(result)['result']['owner_id']
wallet1_id = json.loads(result)['result']['wallet_id']

result = requests.post('http://127.0.0.1:8000/create_owner_and_wallet/',
                       json={'login': 'franz_iscaner', 'name': 'Franz Iscaner'}).text
print(result)
owner2_id = json.loads(result)['result']['owner_id']
wallet2_id = json.loads(result)['result']['wallet_id']

print(requests.post('http://127.0.0.1:8000/topup/', json={'wallet_id': wallet1_id, 'amount': 150.0}).text)

print(requests.post('http://127.0.0.1:8000/make_transfer/',
                    json={'src_wallet_id': wallet1_id, 'dest_wallet_id': wallet2_id, 'amount': 70.0}).text)

print(requests.post('http://127.0.0.1:8000/make_transfer/',
                    json={'src_wallet_id': wallet1_id, 'dest_wallet_id': wallet2_id, 'amount': 70.0}).text)

print(requests.post('http://127.0.0.1:8000/make_transfer/',
                    json={'src_wallet_id': wallet2_id, 'dest_wallet_id': wallet1_id, 'amount': 70.0}).text)

print(requests.post('http://127.0.0.1:8000/make_transfer/',
                    json={'src_wallet_id': wallet1_id, 'dest_wallet_id': wallet2_id, 'amount': 90.0}).text)
