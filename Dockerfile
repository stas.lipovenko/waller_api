FROM python:3.7
WORKDIR /wallet
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8000
COPY . .
ADD start.sh /
RUN chmod +x /start.sh
CMD ["/start.sh"]
