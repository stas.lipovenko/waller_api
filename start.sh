#!/bin/bash

pyton app/models.py
huey_consumer app.huey_transfer_maker.huey &
uvicorn app.api:app --host 0.0.0.0 --port 8000 --reload
