from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel

from app.storage import get_wallet_owner_json, save_wallet_owner, save_wallet, prepare_and_make_transfer, \
    prepare_and_make_topup


class WalletOwnerData(BaseModel):
    login: str
    name: Optional[str]


class WalletData(BaseModel):
    wallet_owner_id: int
    balance: Optional[float] = 0.0


class TransferData(BaseModel):
    src_wallet_id: int
    dest_wallet_id: int
    amount: float


class TopUpData(BaseModel):
    wallet_id: int
    amount: float


app = FastAPI()


@app.post('/create_owner_and_wallet/')
def create_owner_and_wallet(owner_data: WalletOwnerData):
    try:
        owner_id = save_wallet_owner(owner_data.dict())
        wallet_id = save_wallet({'wallet_owner_id': owner_id})

        return {
            'result':
                {
                    'owner_id': owner_id,
                    'wallet_id': wallet_id
                }
        }
    except Exception as e:
        return {'error': e}


@app.get('/wallet_owner_info/{wallet_owner_id}')
def get_wallet_owner_info(wallet_owner_id):
    try:
        return {'result': get_wallet_owner_json(wallet_owner_id)}
    except Exception as e:
        return {'error': e}


# @app.post('/create_wallet/')
# def create_wallet(wallet_data: WalletData):
#     return {'result': save_wallet(wallet_data.dict())}


@app.post('/make_transfer/')
def make_transfer(transfer_data: TransferData):
    try:
        return {'result': prepare_and_make_transfer(transfer_data.dict())}
    except Exception as e:
        return {'error': e}


@app.post('/topup/')
def top_up(topup_data: TopUpData):
    try:
        return {'result': prepare_and_make_topup(topup_data.dict())}
    except Exception as e:
        return {'error': e}
