CONFIG = {
    'db_string': 'sqlite:///wallet.db',#'postgres://wallet:@localhost:54320/wallet',
    'redis': {
        'queue_name': 'tasks.transfers',
        'host': 'redis',
        'port': 6379
    }
}