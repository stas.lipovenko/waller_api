import time

from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, BOOLEAN, CheckConstraint, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy_serializer import SerializerMixin

Base = declarative_base()


class WalletOwner(Base, SerializerMixin):
    __tablename__ = 'wallet_owner'

    id = Column(Integer, primary_key=True)
    login = Column(String(255), nullable=False, unique=True)
    name = Column(String(255))
    wallets = relationship('Wallet')


class Wallet(Base, SerializerMixin):
    __tablename__ = 'wallet'
    serialize_rules = ('-wallet_owner',)

    id = Column(Integer, primary_key=True)
    balance = Column(Float, CheckConstraint('balance >= 0'), nullable=False)
    wallet_owner_id = Column(Integer, ForeignKey('wallet_owner.id'), nullable=False, unique=True)
    wallet_owner = relationship('WalletOwner')
    operations = relationship('WalletOperation')


class WalletOperation(Base, SerializerMixin):
    __tablename__ = 'wallet_operation'
    serialize_rules = ('-wallet',)

    id = Column(Integer, primary_key=True)
    type = Column(String(1), CheckConstraint('type in (\'C\',\'D\')'), nullable=False)
    timestamp = Column(Float, nullable=False, default=time.time())
    amount = Column(Float, nullable=False)
    balance_before = Column(Float, nullable=False)
    status = Column(BOOLEAN, nullable=False)
    src_dest = Column(Integer)
    wallet = relationship('Wallet')
    wallet_id = Column(Integer, ForeignKey('wallet.id'))


if __name__ == '__main__':

    from cfg import CONFIG

    db_string = CONFIG['db_string']

    db_engine = create_engine(db_string)
    Base.metadata.create_all(db_engine)

    from sqlalchemy import MetaData

    m = MetaData()
    m.reflect(db_engine)
    for table in m.tables.values():
        print(table.name)
        for column in table.c:
            print('\t' + column.name)
