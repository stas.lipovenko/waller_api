class ObjectNotFoundException(Exception):
    pass


class InvalidTransferData(Exception):
    pass
