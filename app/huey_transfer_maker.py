from contextlib import contextmanager

from huey import RedisHuey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.cfg import CONFIG

from app.models import Wallet
from app.models import WalletOperation

huey = RedisHuey(
    CONFIG['redis']['queue_name'],
    host=CONFIG['redis']['host'],
    port=CONFIG['redis']['port'],
    results=True
)


def get_session():
    db_string = CONFIG['db_string']

    db_engine = create_engine(db_string)
    Session = sessionmaker(bind=db_engine)

    return Session()


# special context manager to provide transactions support. Besides it closes session itself
@contextmanager
def session_scope():
    session = get_session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


@huey.task()
def make_transfer(src_wallet: Wallet, dest_wallet: Wallet, amount: float):
    with session_scope() as session:
        src_wallet_operation = WalletOperation()
        src_wallet_operation.wallet = src_wallet
        src_wallet_operation.amount = amount
        src_wallet_operation.balance_before = src_wallet.balance
        src_wallet_operation.src_dest = dest_wallet.id
        src_wallet_operation.type = 'C'
        src_wallet_operation.status = False
        session.add(src_wallet_operation)

        dest_wallet_operation = WalletOperation()
        dest_wallet_operation.wallet = dest_wallet
        dest_wallet_operation.amount = amount
        dest_wallet_operation.balance_before = dest_wallet.balance
        dest_wallet_operation.src_dest = src_wallet.id
        dest_wallet_operation.type = 'D'
        dest_wallet_operation.status = False
        session.add(dest_wallet_operation)

        session.commit()

        src_wallet.balance = src_wallet.balance - amount
        dest_wallet.balance = dest_wallet.balance + amount

        src_wallet_operation.status = True
        dest_wallet_operation.status = True


@huey.task()
def make_topup(wallet: Wallet, amount: float):
    with session_scope() as session:
        topup_operation = WalletOperation()
        topup_operation.wallet = wallet
        topup_operation.amount = amount
        topup_operation.balance_before = wallet.balance
        topup_operation.src_dest = None
        topup_operation.type = 'D'
        topup_operation.status = False
        session.add(topup_operation)

        session.commit()

        wallet.balance = wallet.balance + amount

        topup_operation.status = True
