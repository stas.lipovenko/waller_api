from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.exceptions import ObjectNotFoundException, InvalidTransferData
from app.huey_transfer_maker import make_transfer, make_topup
from app.models import WalletOwner, Wallet
from app.cfg import CONFIG


def get_session():
    db_string = CONFIG['db_string']

    db_engine = create_engine(db_string)
    Session = sessionmaker(bind=db_engine)

    return Session()


# special context manager to provide transactions support. Besides it closes session itself
@contextmanager
def session_scope():
    session = get_session()
    try:
        yield session
        session.commit()
    except Exception:
        session.rollback()
        raise
    finally:
        session.close()


def get_wallet_owner(wallet_owner_id):
    with session_scope() as session:
        return session.query(WalletOwner).filter(WalletOwner.id == wallet_owner_id).one_or_none()


def get_wallet_owner_json(wallet_owner_id):
    with session_scope() as session:
        owner = session.query(WalletOwner).filter(WalletOwner.id == wallet_owner_id).one_or_none()
        return owner.to_dict() if owner else 'Not found'


def save_wallet_owner(wallet_owner_data):
    with session_scope() as session:
        owner = WalletOwner()
        owner.login = wallet_owner_data['login']
        owner.name = wallet_owner_data['name']
        session.add(owner)
        session.commit()
        return owner.id


def save_wallet(wallet_data):
    with session_scope() as session:
        wallet = Wallet()
        wallet.wallet_owner = get_wallet_owner(wallet_data['wallet_owner_id'])
        if not wallet.wallet_owner:
            raise ObjectNotFoundException('Owner with such id not found')
        wallet.balance = 0.0
        session.add(wallet)
        session.commit()
        return wallet.id


def prepare_and_make_transfer(transfer_data):
    with session_scope() as session:
        src_wallet = session.query(Wallet).filter(Wallet.id == transfer_data['src_wallet_id']).one_or_none()
        dest_wallet = session.query(Wallet).filter(Wallet.id == transfer_data['dest_wallet_id']).one_or_none()
        amount = transfer_data['amount']
        if not src_wallet or not dest_wallet:
            raise ObjectNotFoundException('One of the wallets with such id not found')
        if amount <= 0:
            raise InvalidTransferData('Transfer amount should be positive')

        make_transfer(src_wallet, dest_wallet, amount)
        return 'Transfer sent to Huey worker'


def prepare_and_make_topup(topup_data):
    with session_scope() as session:
        wallet = session.query(Wallet).filter(Wallet.id == topup_data['wallet_id']).one_or_none()
        amount = topup_data['amount']
        if not wallet:
            raise ObjectNotFoundException('Wallet with such id not found')
        if amount <= 0:
            raise InvalidTransferData('Topup amount should be positive')

        make_topup(wallet, amount)
        return 'Topup sent to Huey worker'
